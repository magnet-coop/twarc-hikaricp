(defproject twarc-hikaricp "0.1.0-SNAPSHOT"
  :description "Testing twarc with HikariCP"
  :url "https://bitbucket.org/magnet-coop/twarc-hikaricp/src"
  :license {:name "Mozilla Public License 2.0"
            :url "https://www.mozilla.org/en-US/MPL/2.0/"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [twarc "0.1.12" :exclusions [org.quartz-scheduler/quartz
                                              org.quartz-scheduler/quartz-jobs
                                              org.clojure/core.async
                                              com.stuartsierra/component]]
                 [org.quartz-scheduler/quartz "2.3.0" :exclusions [com.mchange/c3p0
                                                                   com.zaxxer/HikariCP-java6
                                                                   org.slf4j/slf4j-api]]
                 [org.quartz-scheduler/quartz-jobs "2.3.0"]
                 [duct/database.sql.hikaricp "0.3.3"]
                 [org.postgresql/postgresql "9.4.1212"]]
  :profiles {:repl {:plugins [[cider/cider-nrepl "0.18.0"]]}})
