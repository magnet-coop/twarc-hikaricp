(ns twarc-hikaricp.core-test
  (:require [clojure.test :refer :all]
            [integrant.core :as ig]
            [duct.database.sql.hikaricp]))

(def config
  {:duct.database.sql/hikaricp {:jdbc-url "jdbc:postgresql://db:5432/twarc?user=twarc&password=password"
                                :maximum-pool-size 1}})


(deftest ^:integration test-twarc
  (testing "Trying to use HikariCP")
  (let [system (ig/init config)]
    (is (= 1 1))
   (ig/halt! system)))
