# twarc-hikaricp

Testing twarc library with a new version of Quartz that support HikariCP

## License

Copyright © 2018 Magnet Coop

Distributed under the Mozilla Public License 2.0
